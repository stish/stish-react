import React from 'react'

import {
  Plugin,
  StishStorage,
  StishLoader,
  PluginLifecycle,
  forceEnablePlugin
} from 'stish-react'

const App = () => {
  console.log({
    Plugin,
    StishStorage,
    StishLoader,
    PluginLifecycle,
    forceEnablePlugin
  })
  return <div>STISH TEST</div>
}

export default App
