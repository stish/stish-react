import React from 'react'
import styles from './styles.module.css'

import {
  Plugin,
  StishStorage,
  StishLoader,
  PluginLifecycle,
  forceEnablePlugin
} from './stish'

// export const ExampleComponent = ({ text }) => {
//   console.log(Plugin)
//   return <div className={styles.test}>Example Componen 2t: {text}</div>
// }

export { Plugin, StishStorage, StishLoader, PluginLifecycle, forceEnablePlugin }
