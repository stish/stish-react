import React, { useState, useEffect, Fragment } from "react";
import StishStorage from "./StishStorage";

// I handle the DOM for all plugins
// All installed plugins get a dom node regardless if they are enabled or not
// But, the component inside of each plugin, does not mount unless the component is enabled

const StishLoader = () => {
  const [allPlugins, _allPlugins] = useState({});

  const fetchPlugins = () => {
    // Lets fetch the list of all available plugins
    fetch("/api/plugins")
      .then((d) => d.json())
      .then((data) => {
        // Lets merge the list of plugins installed with the list of plugins enabled/disabled in localStishStorage
        // if its a new plugin it will be set to disabled(false) by default
        let storedPluginState = StishStorage.getItem("stishPlugins");

        let newPluginsState = {};
        Object.keys(data.plugins).map(
          (key) =>
            (newPluginsState[data.plugins[key].replace(".js", "")] =
              storedPluginState &&
              storedPluginState[data.plugins[key].replace(".js", "")]
                ? storedPluginState[data.plugins[key].replace(".js", "")]
                : false)
        );
        _allPlugins(newPluginsState);
      });
  };

  useEffect(() => {
    // If the browser is missing the stishPlugins localStishStorage key, then lets make it
    if (!StishStorage.getItem("stishPlugins")) {
      StishStorage.setItem(
        "stishPlugins",
        // We will need this plugin set to true so we can manage the plugin later
        { StishManager: true }
      );
    }

    // Lets fetch the list of all available plugins
    fetchPlugins();

    const handleStishMessage = (message) =>
      message.data.type === "ls-stishPluginsUpdated" &&
      _allPlugins(StishStorage.getItem("ls-stishPluginsUpdated"));

    window.addEventListener("message", handleStishMessage);
    return () => {
      window.removeEventListener("message", handleStishMessage);
    };
  }, []);
  return (
    <Fragment>
      {/* Load Dependencies For All Plugins (enabled and disabled) */}
      {Object.keys(allPlugins).length > 0 &&
        Object.keys(allPlugins).map((plugin, i) => {
          if (!document.getElementById(`--plugin-${plugin}`)) {
            let pluginToInsert = document.createElement("div");
            pluginToInsert.id = `--plugin-${plugin}`;
            document.body.append(pluginToInsert);
            if (!document.getElementById(`--plugin-script-${plugin}`)) {
              var newScript = document.createElement("script");
              newScript.src = "./plugins/" + plugin + ".js";
              newScript.id = `--plugin-script-${plugin}`;
              document.body.appendChild(newScript);
            }
          }
        })}
    </Fragment>
  );
};

export default StishLoader;
