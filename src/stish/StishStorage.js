const StishStorage = {
  getItem: (key) => {
    return JSON.parse(localStorage.getItem(key));
  },
  setItem: (key, value, overrides) => {
    localStorage.setItem(key, JSON.stringify(value));

    window.postMessage(
      {
        type: "ls-stishPluginsUpdated",
        ...overrides,
      },
      "*"
    );
  },
};

export default StishStorage;
