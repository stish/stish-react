import React, { useState, useEffect, Fragment } from "react";
import ReactDOM from "react-dom";
import PluginLifecycle from "./PluginLifecycle";
import StishStorage from "./StishStorage";

const Plugin = (config) => {
  const Comp = config.component;
  let pluginName = config.name || config.component.name;

  const getSettingsForPlugin = (pluginName) => {
    let pluginStorageKey = `--plugin-setting-${pluginName}`;
    let findPluginSettingsStored = StishStorage.getItem(pluginStorageKey);
    if (findPluginSettingsStored) {
      return findPluginSettingsStored.storage;
    } else {
      // Create a block for this plugin

      const newUserStorage = {};

      for (const key of Object.keys(config.settings)) {
        newUserStorage[key] = null;
      }

      StishStorage.setItem(pluginStorageKey, {
        schema: config.settings,
        storage: newUserStorage,
      });

      return findPluginSettingsStored.storage;
    }
  };

  const WrappedPluginLifecycle = PluginLifecycle(Comp, pluginName, config);

  const mounter = () => {
    const pluginNode = document.getElementById(`--plugin-${pluginName}`);

    // const root = ReactDOM.createRoot(pluginNode);

    // Since components do not share each others configs, but stishmanager needs this to render the metadata
    // we send these details on page load as a postMessage
    if (!StishStorage.getItem("stishMeta")) {
      StishStorage.setItem("stishMeta", {});
    }
    let currentMeta = StishStorage.getItem("stishMeta");
    currentMeta[pluginName] = {
      description: config.description || "",
      category: config.styles ? "themes" : "add-ons",
    };
    StishStorage.setItem("stishMeta", currentMeta);

    window.postMessage(
      {
        type: "ls-stishManagerMetaData",
        // meta: config,
        meta: {
          name: pluginName,
          description: config.description || "",
          category: config.styles ? "themes" : "add-ons",
        },
      },
      "*"
    );

    // root.render(
    //   <WrappedPluginLifecycle
    //     execRoute={config.route}
    //     settings={config.settings ? getSettingsForPlugin(pluginName) : {}}
    //   />
    // );

    ReactDOM.render(
      <WrappedPluginLifecycle
        execRoute={config.route}
        settings={config.settings ? getSettingsForPlugin(pluginName) : {}}
      />,
      pluginNode
    );
  };
  mounter();
};

export default Plugin;
