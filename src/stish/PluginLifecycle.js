import React, { Fragment } from "react";
import StishStorage from "./StishStorage";

/*
messagingComponent HOC
//
this little HOC can be used to allow plugins to communicate with each other
using localStishStorage and messaging

example 

first argument is the key you want to subscribe and post to, and the second is the <Component />
messagingComponent("sitestate", PluginComponentHere);




*/

const routeMatcher = (configRoute) => {
  var route = require("path-match")({
    // path-to-regexp options
    sensitive: true,
    strict: true,
    end: false,
  });
  // match a route
  var parse = require("url").parse;
  // create a match function from a route
  var match = route(configRoute);

  var matchBool = match(parse(window.location.href).pathname);

  return matchBool;
};

export default function PluginLifecycle(WrappedComponent, pluginName, config) {
  class LifecycleHOC extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        enabled: false,
        settings: this.props.settings,
        routeMatcher: routeMatcher(config.route) !== false,
      };
      this.displayName = "Stishy";
    }

    getSettingsForPlugin = (pluginName) => {
      let pluginStishStorageKey = `--plugin-setting-${pluginName}`;
      let findPluginSettingsStored = StishStorage.getItem(
        pluginStishStorageKey
      );
      if (findPluginSettingsStored) {
        return findPluginSettingsStored.StishStorage;
      } else {
        // Create a block for this plugin

        const newUserStishStorage = {};

        for (const key of Object.keys(config.settings)) {
          newUserStishStorage[key] = null;
        }

        StishStorage.setItem(pluginStishStorageKey, {
          schema: config.settings,
          StishStorage: newUserStishStorage,
        });

        return findPluginSettingsStored.StishStorage;
      }
    };

    componentDidMount() {
      if (StishStorage.getItem("stishPlugins")) {
        if (StishStorage.getItem("stishPlugins")[pluginName]) {
          this.setState({
            ...this.state,
            enabled: StishStorage.getItem("stishPlugins")[pluginName],
          });
        }
      }

      window.addEventListener("StishStorage", () =>
        this.onStishStorageChange()
      );
      window.addEventListener("message", (message) =>
        this.onPostMessage(message)
      );
    }

    componentWillUnmount() {
      window.removeEventListener("message", (message) =>
        this.onPostMessage(message)
      );
      window.removeEventListener("StishStorage", () =>
        this.onStishStorageChange()
      );
    }

    isPluginEnabled = (pluginName) => {
      let stishPlugins = StishStorage.getItem("stishPlugins");
      return stishPlugins[pluginName];
    };

    // We cant detect StishStorage changes in the same window so well listen to message
    // then call onStishStorageChange manually if its in the same window
    onPostMessage = (message) => {
      if (
        message.data.type === "ls-stishPluginsUpdated" ||
        message.data.type === "ls-stishNavigationUpdate"
      ) {
        this.onStishStorageChange();
      }
    };

    // What do we do when StishStorage changes?
    onStishStorageChange = () => {
      this.setState({
        ...this.state,
        routeMatcher: routeMatcher(config.route) !== false,
      });

      if (
        this.state.enabled !==
          StishStorage.getItem("stishPlugins")[pluginName] ||
        JSON.stringify(this.state.settings) !==
          JSON.stringify(StishStorage.getItem(`--plugin-setting-${pluginName}`))
      ) {
        this.setState({
          settings: StishStorage.getItem(`--plugin-setting-${pluginName}`),
          enabled: StishStorage.getItem("stishPlugins")[pluginName],
        });
      }
    };

    render() {
      return (
        <Fragment>
          {this.state.enabled &&
            config.component &&
            this.state.routeMatcher && <WrappedComponent {...this.state} />}
          {this.state.enabled && config.styles && this.state.routeMatcher && (
            <style id={`--plugin-styles-${pluginName}`}>{config.styles}</style>
          )}
        </Fragment>
      );
    }
  }
  LifecycleHOC.displayName = pluginName;
  return LifecycleHOC;
}
