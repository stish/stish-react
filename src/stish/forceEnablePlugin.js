import StishStorage from "./StishStorage";
const forceEnablePlugin = (pluginName) => {
  // WARNING: This should not be done in any plugin
  // We will automatically enable out plugin when its mounted the first time
  let plugins = StishStorage.getItem("stishPlugins") || {};
  if (!StishStorage.getItem("stishPlugins")[pluginName]) {
    StishStorage.setItem("stishPlugins", {
      ...plugins,
      [pluginName]: true,
    });
  }
};

export default forceEnablePlugin;
