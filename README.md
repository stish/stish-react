# stish-react

> a library for building stish plugins

[![NPM](https://img.shields.io/npm/v/stish-react.svg)](https://www.npmjs.com/package/stish-react) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save stish-react
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'stish-react'
import 'stish-react/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
